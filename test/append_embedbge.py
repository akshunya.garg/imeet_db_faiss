from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.vectorstores import FAISS
from langchain.embeddings import HuggingFaceBgeEmbeddings
from langchain.document_loaders import TextLoader
import os
openai_api_key="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"
os.environ["OPENAI_API_KEY"] = "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"
loader = TextLoader('state_of_the_union.txt', encoding='utf-8')
documents = loader.load()
text_splitter=RecursiveCharacterTextSplitter(chunk_size=1000,chunk_overlap=80)
texts=text_splitter.split_documents(documents)
model_name = "BAAI/bge-base-en"
encode_kwargs = {'normalize_embeddings': True}
embeddings = HuggingFaceBgeEmbeddings(
    model_name=model_name,
    model_kwargs={'device': 'cpu'},
    encode_kwargs=encode_kwargs
)
faiss_db = FAISS.from_documents(texts, embeddings)
faiss_db.save_local("new_index1")
loader = TextLoader('RussiaUkraineWar.txt', encoding='utf-8')
documents1 = loader.load()
text_splitter=RecursiveCharacterTextSplitter(chunk_size=1000,chunk_overlap=80)
texts1 = text_splitter.split_documents(documents)
faiss_db1=FAISS.from_documents(texts1,embeddings)
if (os.path.exists("new_index1")):
    local_db = FAISS.load_local("new_index1", embeddings)
    local_db.merge_from(faiss_db1)
    print("Merge Completed")   
    local_db.save_local("new_index1")
    print("Updated index saved")
else:
    faiss_db1.save_local("new_index1")
    print("New Store created...")
